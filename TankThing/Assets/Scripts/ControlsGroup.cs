﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ControlsGroup {
    [System.Serializable]
    public struct ControlsKeyboard
    {
        public KeyCode moveForward;
        public KeyCode moveBackward;
        public KeyCode moveLeft;
        public KeyCode moveRight;
        public KeyCode shoot;
    }

    [System.Serializable]
    public struct ControlsGamePad
    {
        public KeyCode moveForward;
        public KeyCode moveBackward;
        public KeyCode moveLeft;
        public KeyCode moveRight;
        public KeyCode shoot;
    }

    public ControlsKeyboard keyboard;
    public ControlsGamePad controller1;
    public ControlsGamePad controller2;
    public ControlsGamePad controller3;
    public ControlsGamePad controller4;
}
