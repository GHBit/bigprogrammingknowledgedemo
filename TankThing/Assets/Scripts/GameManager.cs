﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: Add win/lose states to manager and handle them
//TODO: Add PvE and PvP modes, rather than just PvE
public class GameManager : MonoBehaviour {
    //The game manager
    public static GameManager gameManager;

    //Controls
    public ControlsGroup controls;

    //List of characters
    public List<CharacterData> playerCharacters;
    public List<CharacterData> aiCharacters;

    //Sets up game manager and prevents duplicates
    private void Awake()
    {
        if (gameManager == null)
        {
            gameManager = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            Debug.Log("Game manager already present, from GameManager.cs");
        }
    }

    private void Start()
    {
        //populates list of characters for both players and AI
        foreach (PlayerController character in GameObject.FindObjectsOfType<PlayerController>())
        {
            playerCharacters.Add(character.GetComponent<CharacterData>());
        }
        foreach (AIController character in GameObject.FindObjectsOfType<AIController>())
        {
            aiCharacters.Add(character.GetComponent<CharacterData>());
        }
    }
}
