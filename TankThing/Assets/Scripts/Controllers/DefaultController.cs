﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterData))]

public class DefaultController : MonoBehaviour {
    //Character data component
    protected CharacterData data;

    //amount of time since last bullet was fired
    protected float timeOfLastBullet;

    // Use this for initialization
    protected virtual void Start()
    {
        data = GetComponent<CharacterData>();
    }

    //Checks to see if bullet can be fired, then fires bullet and resets timer
    protected virtual void ShootMainBullet()
    {
        if (Time.time - timeOfLastBullet >= data.firingRate || data.canFireInfiniteBulletsAtOnce)
        {
            if (!data.doOverrideBulletsDamageMod)
                data.shooter.ShootBullet(data.bullet, data.bulletData);
            else
                data.shooter.ShootBulletAndOverrideDamage(data.bullet, data.bulletData, data.bulletDamageMod);
            timeOfLastBullet = Time.time;
        }
    }
}
