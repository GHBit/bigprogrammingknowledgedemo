﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BulletData))]

public class BulletController : DefaultController {
    private BulletData bulletData;

    private float timeOfSpawn;
	// Use this for initialization
	protected override void Start() {
        bulletData = GetComponent<BulletData>();
        bulletData.myRigidBody.AddForce(bulletData.myTransform.forward * bulletData.forceToApplyToBullet);
        timeOfSpawn = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time > timeOfSpawn + bulletData.duration)
            Destroy(gameObject);
	}
}
