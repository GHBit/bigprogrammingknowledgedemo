﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : DefaultController
{
    //controls layout
    private static ControlsGroup controls;

    //Input Device being used (defaults to keyboard)
    public enum InputDevice { keyboard, controller };
    public InputDevice inputDevice = InputDevice.keyboard;

    //current controller if controller is being used
    private ControlsGroup.ControlsGamePad currentGamepad;

    protected override void Start()
    {
        base.Start();

        controls = GameManager.gameManager.controls;
        currentGamepad = controls.controller1;
        timeOfLastBullet = -data.firingRate;
    }

    // Update is called once per frame
    void Update()
    {
        switch (inputDevice)
        {
            case InputDevice.keyboard:
                if (data != null)
                {
                    MoveKeyboard();
                    RotateKeyboard();
                    if (Input.GetKeyDown(controls.keyboard.shoot))
                        ShootMainBullet();
                }
                break;
        }
    }

    private void MoveKeyboard()
    {
        if (Input.GetKey(controls.keyboard.moveForward))
            data.mover.Movement(data.movementSpeed, 0);
        else if (Input.GetKey(controls.keyboard.moveBackward))
            data.mover.Movement(-data.movementSpeed, 0);
        else //makes sure gravity is applied every frame
            data.mover.Movement(0, 0);
    }

    private void RotateKeyboard()
    {
        if (Input.GetKey(controls.keyboard.moveLeft))
            data.mover.Rotation(-data.turnSpeed);
        else if (Input.GetKey(controls.keyboard.moveRight))
            data.mover.Rotation(data.turnSpeed);
    }
}

