﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterData))]

public class AIController : DefaultController {

	// Update is called once per frame
	void Update () {
        //TODO: Update AI to do more than move and rotate every frame
        data.mover.Movement(data.movementSpeed, 0);
        data.mover.Rotation(data.turnSpeed);
        ShootMainBullet();
    }
}
