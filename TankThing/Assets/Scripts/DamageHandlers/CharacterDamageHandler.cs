﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterData))]

public class CharacterDamageHandler : MonoBehaviour
{
    private CharacterData data;

    // Use this for initialization
    void Start()
    {
        data = GetComponent<CharacterData>();
    }

    //bool returns if the character died
    public bool TakeDamageAndReturnTrueIfOutOfHealth(float damage)
    {
        data.currentHP -= damage;
        Debug.Log(data.currentHP);
        if (data.currentHP <= 0)
            return true;
        else
            return false;
    }
}
