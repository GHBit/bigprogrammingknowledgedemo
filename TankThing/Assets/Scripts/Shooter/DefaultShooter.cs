﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterData))]

public class DefaultShooter : MonoBehaviour {
    private CharacterData data;

    private void Start()
    {
        data = GetComponent<CharacterData>();
    }

    public void ShootBullet(GameObject bullet, BulletData bulletData)
    {
        //Sets the creator of the bullet
        bulletData.objectThatCreatedBullet = gameObject;
        bulletData.dataOfObjectThatCreatedBullet = data;

        Vector3 bulletPosition;
        Quaternion bulletRotation;

        bulletPosition = data.myTransform.forward * data.myCollider.bounds.extents.z * data.myTransform.localScale.z + data.myTransform.position;
        bulletRotation = data.myTransform.rotation;

        Instantiate(bullet, bulletPosition, bulletRotation);
    }

    public void ShootBulletAndOverrideDamage(GameObject bullet, BulletData bulletData, float damage)
    {
        float originalBulletDamage = bulletData.damage;

        bulletData.damage = damage; //sets the damage for the bullet prefab
        ShootBullet(bullet, bulletData); //instantiates the bullet
        bulletData.damage = originalBulletDamage; //prevents the change from being permanent in the editor
    }
}
