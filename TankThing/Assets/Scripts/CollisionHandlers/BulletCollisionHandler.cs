﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BulletData))]

public class BulletCollisionHandler : MonoBehaviour {
    private BulletData data;

	// Use this for initialization
	void Start () {
        data = GetComponent<BulletData>();
	}

    //Checks to see if object can take damage and is not the object that spawned the bullet
    //Then deals damage
    //Then if the collided object's health is 0 it destroys the object and adds to the player's score
    //Then self destructs if the object isn't the character that spawned the bullet
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject != data.objectThatCreatedBullet)
        {
            bool didKill = false;
            CharacterData characterData = collision.gameObject.GetComponent<CharacterData>();
            if (characterData != null)
            {
                didKill = characterData.damageHandler.TakeDamageAndReturnTrueIfOutOfHealth(data.damage);
            }
            if (didKill == true)
            {
                data.dataOfObjectThatCreatedBullet.score += characterData.scoreRewardOnDeath;
                Destroy(collision.gameObject);
            }
            Debug.Log(data.dataOfObjectThatCreatedBullet.score);
            Destroy(gameObject);
        }
    }
}
