﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterData))]

public class MovementCore : MonoBehaviour
{
    protected CharacterData data;

    protected virtual void Start()
    {
        data = GetComponent<CharacterData>();
    }

    public virtual void Movement(float forwardDirectionAndSpeed, float sidewaysDirectionAndSpeed)
    {
        data.simpleMoveController.SimpleMove(transform.forward * forwardDirectionAndSpeed + transform.right * sidewaysDirectionAndSpeed);
    }

    public virtual void Rotation(float directionAndSpeed)
    {
        data.myTransform.Rotate(Vector3.up * directionAndSpeed * Time.deltaTime, Space.Self);
    }
}
