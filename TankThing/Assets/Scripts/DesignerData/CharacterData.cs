﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DefaultController))]
[RequireComponent(typeof(MovementCore))]
[RequireComponent(typeof(DefaultShooter))]
[RequireComponent(typeof(CharacterDamageHandler))]
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Collider))]

public class CharacterData : MonoBehaviour {
    //public designer variables
    public float movementSpeed;
    public float turnSpeed;
    public float fallSpeed;
    public float firingRate;
    public float maxHP;
    public float scoreRewardOnDeath = 1;
    public bool canFireInfiniteBulletsAtOnce;
    public bool doOverrideBulletsDamageMod;
    public float bulletDamageMod;

    //The bullet that the tank fires
    public GameObject bullet;
    [HideInInspector]
    public BulletData bulletData;

    [HideInInspector]
    public float currentHP;
    //[HideInInspector]
    public float score;

    //components
    [HideInInspector]
    public DefaultController controller;
    [HideInInspector]
    public MovementCore mover;
    [HideInInspector]
    public DefaultShooter shooter;
    [HideInInspector]
    public CharacterDamageHandler damageHandler;
    [HideInInspector]
    public CharacterController simpleMoveController;
    [HideInInspector]
    public Transform myTransform;
    [HideInInspector]
    public Collider myCollider;

    private void Start()
    {
        bulletData = bullet.GetComponent<BulletData>();
        controller = GetComponent<DefaultController>();
        mover = GetComponent<MovementCore>();
        shooter = GetComponent<DefaultShooter>();
        damageHandler = GetComponent<CharacterDamageHandler>();
        simpleMoveController = GetComponent<CharacterController>();
        myTransform = GetComponent<Transform>();
        myCollider = GetComponent<Collider>();

        currentHP = maxHP;
    }
}
