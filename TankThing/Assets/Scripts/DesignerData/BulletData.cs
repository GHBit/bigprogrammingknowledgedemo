﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BulletController))]
[RequireComponent(typeof(Rigidbody))]

public class BulletData : MonoBehaviour {
    public float forceToApplyToBullet;
    public float damage;
    public float duration;

    [HideInInspector]
    public BulletController bulletController;
    [HideInInspector]
    public Rigidbody myRigidBody;
    [HideInInspector]
    public Transform myTransform;
    [HideInInspector]
    public GameObject objectThatCreatedBullet; //prevents bullets from destroying their creator
    [HideInInspector]
    public CharacterData dataOfObjectThatCreatedBullet; //helps set score

    private void Start()
    {
        bulletController = GetComponent<BulletController>();
        myRigidBody = GetComponent<Rigidbody>();
        myTransform = GetComponent<Transform>();
    }
}
